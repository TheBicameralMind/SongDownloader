package com.mynetgear.achaplin.songdownloader.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CharonWebService {

    @GET("metadata/md_{collection_b64}.json")
    Single<Object> getCollectionMetadata(@Path("collection_b64") String col_b64);

    static CharonWebService createCharonWebService() {
        return new Retrofit.Builder()
                .baseUrl("http://achaplin.mynetgear.com:8080/charon-web/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(CharonWebService.class);
    }

    static List<String> getCollectionValues(Object o) {
        //noinspection unchecked
        return new ArrayList<>(((Map<String, String>) o).values());
    }

    static List<String> getCollectionKeys(Object o) {
        //noinspection unchecked
        return new ArrayList<>(((Map<String, String>) o).keySet());
    }
}
