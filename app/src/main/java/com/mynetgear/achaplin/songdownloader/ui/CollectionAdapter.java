package com.mynetgear.achaplin.songdownloader.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mynetgear.achaplin.songdownloader.R;
import com.mynetgear.achaplin.songdownloader.util.Misc;

import java.util.List;
import java.util.Set;

public class CollectionAdapter extends RecyclerView.Adapter<CollectionViewHolder> {

    private List<String> collectionTitles;
    private Set<String> toSync;

    public CollectionAdapter(List<String> data, Set<String> toSync) {
        collectionTitles = data;
        this.toSync = toSync;
    }

    @NonNull
    @Override
    public CollectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.collection_item, parent, false);
        return new CollectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CollectionViewHolder holder, int position) {
        //TODO textview doesn't exist to change values apparently? 
        holder.checkBox.setText(collectionTitles.get(position));
        holder.checkBox.setOnCheckedChangeListener((view, checked) -> {
            if (checked) {
                toSync.add(Misc.toBase64(holder.checkBox.getText().toString()));
                Misc.incColSyncNum(holder.itemView.getRootView(), 1);
            } else {
                toSync.remove(Misc.toBase64(holder.checkBox.getText().toString()));
                Misc.incColSyncNum(holder.itemView.getRootView(), -1);
            }
        });
        if (toSync.contains(Misc.toBase64(collectionTitles.get(position)))) {
//            Misc.incColSyncNum(holder.itemView.getRootView(), 1);
            holder.checkBox.setChecked(true);
        }
    }

    @Override
    public int getItemCount() {
        return collectionTitles.size();
    }

    public void setData(List<String> data) {
        collectionTitles = data;
    }
}
