package com.mynetgear.achaplin.songdownloader.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mynetgear.achaplin.songdownloader.R;
import com.mynetgear.achaplin.songdownloader.api.CharonWebService;
import com.mynetgear.achaplin.songdownloader.util.Misc;
import com.mynetgear.achaplin.songdownloader.viewmodel.DownloadModel;
import com.mynetgear.achaplin.songdownloader.viewmodel.DownloadViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CollectionsFragment extends Fragment {

    @BindView(R.id.collection_list)
    public RecyclerView collectionList;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Unbinder unbinder;
    private Context context;
    private CollectionAdapter collectionAdapter;

    private DownloadModel model;

    public CollectionsFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.collections_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);

        collectionList.setHasFixedSize(true);
        collectionList.setLayoutManager(new LinearLayoutManager(context));

        model = ViewModelProviders.of(this).get(DownloadViewModel.class).getDownloadModel();

        collectionAdapter = new CollectionAdapter(new ArrayList<>(), model.getCollectionsToSync());
        collectionList.setAdapter(collectionAdapter);
        getCollections();

        return view;
    }

    @Override
    public void onDestroy() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
        unbinder.unbind();
        super.onDestroy();
    }

    public void getCollections() {
        CharonWebService charonWebService = CharonWebService.createCharonWebService();
        charonWebService.getCollectionMetadata("collections")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Object>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(Object o) {
                        collectionAdapter.setData(CharonWebService.getCollectionValues(o));
                        collectionAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Misc.shortToast(e.getMessage(), context);
                    }
                });
    }

    @OnClick(R.id.button2)
    public void collectionsSync() {
        model.writeCurrentCollections(context);
        //TODO enqueue songs (and hold references to uris?)
//        Set<String> toSync = model.getCollectionsToSync();
//        List<String> remoteSongs = new ArrayList<>();
//        CharonWebService charonWebService = CharonWebService.createCharonWebService();
//        for (String url : toSync) {
//            charonWebService.getCollectionMetadata(url)
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new SingleObserver<Object>() {
//                        @Override
//                        public void onSubscribe(Disposable d) {
//                            compositeDisposable.add(d);
//                        }
//
//                        @Override
//                        public void onSuccess(Object o) {
//                            remoteSongs.addAll(CharonWebService.getCollectionKeys(o));
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
//                        }
//                    });
//        }
//        if (Permissions.readAndWriteExternalStorage(getContext())) {
//        String url = "http://achaplin.mynetgear.com:8080/charon-web/collections/RURN/songs/S2lzbWEgLSBGaW5nZXJ0aXBz";
//        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
//        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "file.mp3");
//        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
//        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//        DownloadManager downloadManager = (DownloadManager) getContext().getSystemService(Context.DOWNLOAD_SERVICE);
//        downloadManager.enqueue(request);
    }
}
