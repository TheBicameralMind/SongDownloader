package com.mynetgear.achaplin.songdownloader.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mynetgear.achaplin.songdownloader.R;
import com.mynetgear.achaplin.songdownloader.util.Permissions;
import com.mynetgear.achaplin.songdownloader.viewmodel.DownloadViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.tabLayout)
    public TabLayout tabLayout;

    @BindView(R.id.viewpager)
    public ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // set up swipe to change tabs
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        Permissions.readAndWriteExternalStorage(this);

        // populate the currently synced collections
        ViewModelProviders.of(this).get(DownloadViewModel.class).getDownloadModel().readCurrentCollections(getApplicationContext());

//        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CollectionsFragment(), "Collections");
        adapter.addFragment(new SyncFragment(), "Sync");
        viewPager.setAdapter(adapter);
    }
}
