package com.mynetgear.achaplin.songdownloader.viewmodel;

import android.content.Context;
import android.os.Environment;

import com.mynetgear.achaplin.songdownloader.util.Misc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DownloadModel {

    private static DownloadModel downloadModel = new DownloadModel();

    public static DownloadModel getDownloadModel() {
        return downloadModel;
    }

    private Set<String> collectionsToSync;
    private List<File> currentSongs;
    private ExecutorService threadPool;

    public DownloadModel() {
        collectionsToSync = new HashSet<>();
        currentSongs = new ArrayList<>();
        threadPool = Executors.newFixedThreadPool(4);
    }

    public Set<String> getCollectionsToSync() {
        return collectionsToSync;
    }


    public void readCurrentCollections(Context context) {
        try {
            FileInputStream fis = context.openFileInput("currentCollections.sy");
            ObjectInputStream ois = new ObjectInputStream(fis);
            this.collectionsToSync = (HashSet<String>) ois.readObject();
        } catch (FileNotFoundException e) {
            Misc.shortToast("serialization not found", context);
            this.collectionsToSync = new HashSet<>();
        } catch (IOException | ClassNotFoundException e) {
            Misc.longToast(e.getLocalizedMessage(), context);
        }
    }

    public void writeCurrentCollections(Context context) {
        try {
            FileOutputStream fos = context.openFileOutput("currentCollections.sy", context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(this.collectionsToSync);
            os.close();
            fos.close();
        } catch (IOException e) {
            Misc.longToast(e.getLocalizedMessage(), context);
        }
    }

    public boolean sync(String colB64) {
        return collectionsToSync.contains(colB64);
    }

    public ExecutorService getThreadPool() {
        return threadPool;
    }

    public void refreshSongCount() {
        currentSongs = new ArrayList<>(Arrays.asList(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).listFiles()));
    }

    public String localSongs() {
        return currentSongs.size() + " local songs";
    }
}
