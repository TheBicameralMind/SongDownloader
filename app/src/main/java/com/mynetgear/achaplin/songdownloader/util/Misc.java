package com.mynetgear.achaplin.songdownloader.util;

import android.content.Context;
import android.util.Base64;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mynetgear.achaplin.songdownloader.R;

public class Misc {

    public static String toBase64(String encode) {
        return Base64.encodeToString(encode.getBytes(), Base64.NO_WRAP);
    }

    public static String fromBase64(String decode) {
        return String.valueOf(Base64.decode(decode, Base64.NO_WRAP));
    }

    public static void shortToast(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void longToast(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void incColSyncNum(View itemView, int i) {
        TextView tv = itemView.findViewById(R.id.col_number_summary);
        String[] curText = ((String) tv.getText()).split(" ");
        curText[1] = Integer.toString(Integer.parseInt(curText[1]) + i);
        curText[2] = curText[1].equals("1") ? "collection" : "collections";
        tv.setText(String.join(" ", curText));
    }
}
