package com.mynetgear.achaplin.songdownloader.viewmodel;

import androidx.lifecycle.ViewModel;

public class DownloadViewModel extends ViewModel {
    private DownloadModel downloadModel;

    public DownloadViewModel() {
        downloadModel = DownloadModel.getDownloadModel();
    }

    public DownloadModel getDownloadModel() {
        return downloadModel;
    }
}
