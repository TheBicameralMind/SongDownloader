package com.mynetgear.achaplin.songdownloader.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.mynetgear.achaplin.songdownloader.R;
import com.mynetgear.achaplin.songdownloader.util.Misc;
import com.mynetgear.achaplin.songdownloader.viewmodel.DownloadModel;
import com.mynetgear.achaplin.songdownloader.viewmodel.DownloadViewModel;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

public class SyncFragment extends Fragment {

    private Unbinder unbinder;

    private DownloadModel model;

    @BindView(R.id.song_number_summary)
    public TextView numberSummary;

    @BindView(R.id.download_list)
    public RecyclerView downloadList;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public SyncFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        model = ViewModelProviders.of(this).get(DownloadViewModel.class).getDownloadModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sync_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);

        Future async = model.getThreadPool().submit(() -> {
           model.refreshSongCount();
           return model.localSongs();
        });

        try {
            numberSummary.setText((String) async.get());
        } catch (ExecutionException a) {
            Misc.longToast(a.getLocalizedMessage(), getContext());
        } catch (InterruptedException a) {
            Misc.longToast(a.getLocalizedMessage(), getContext());
        }

        return view;
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }

        super.onDestroy();
    }

    @OnClick(R.id.button)
    public void cancel() {
        //TODO cancel ongoing and queued downloads
    }
}
