package com.mynetgear.achaplin.songdownloader.ui;

import android.view.View;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mynetgear.achaplin.songdownloader.R;

public class CollectionViewHolder extends RecyclerView.ViewHolder {

    public CheckBox checkBox;

    public CollectionViewHolder(@NonNull View itemView) {
        super(itemView);
        checkBox = itemView.findViewById(R.id.collection_select_box);
    }


}
